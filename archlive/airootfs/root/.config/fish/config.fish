set fish_greeting ""
set TERM "xterm-256color"
set EDITOR "/usr/bin/nvim"

if status is-interactive
    # Commands to run in interactive sessions can go here
end

alias sudo="doas"

alias vim="nvim"
alias ss="doas systemctl"
alias v="vim"
alias n="nano"
alias sv="doas nvim"
alias sn="doas nano"
alias dnf="doas dnf"

# Colors
alias ls="ls -hN --color=auto --group-directories-first"
alias grep="grep --color=auto"
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias cat="highlight --out-format=ascii"

# Confirm before overwriting something
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'

# "ls" to "exa"
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing
alias l.='exa -a | egrep "^\."'

# NVim swap shit
alias vimsw="rm -v ~/.local/share/nvim/swap/*" 

yafetch
#sl
